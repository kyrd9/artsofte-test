let data = {carousel: {
    minicar: [
      {mark: 'Civic 4D', stock: true, img: 'minicar1.png', price: 'от 700 000 руб.'},
      {mark: 'Civic 5D', stock: true, img: 'minicar2.png', price: 'от 599 000 руб.'},
      {mark: 'CR-V 2.0', stock: true, img: 'minicar3.png', price: 'от 1 179 000 руб.'},
      {mark: 'CR-V 2.4', stock: true, img: 'minicar4.png', price: 'от 1 250 000 руб.'},
      {mark: 'Pilot', stock: true, img: 'minicar5.png', price: 'от 2 115 000 руб.'},
      {mark: 'Accord', img: 'minicar6.png', price: 'от 800 000 руб.'},
      {mark: 'Crosstour', img: 'minicar7.png', price: 'от 1 650 000 руб.'}
    ]
}};
