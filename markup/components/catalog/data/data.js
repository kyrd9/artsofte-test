let data = {catalog: {
    car: [
      {mark: 'Civic 4D', img: 'car01.png', price: 'ОТ 590 000 руб.'},
      {mark: 'Civic 5D', img: 'car02.png', price: 'ОТ 680 000 руб.'},
      {mark: 'CR-V 2.0', img: 'car03.png', price: 'ОТ 853 000 руб.'},
      {mark: 'CR-V 2.4', img: 'car04.png', price: 'ОТ 1 179 000 руб.'},
      {mark: 'PILOT', img: 'car05.png', price: 'ОТ 1 755 000 руб.'},
      {mark: 'ACCORD', img: 'car06.png', price: 'ОТ 1 082 000 руб.'},
      {mark: 'Crosstour', img: 'car007.png', price: 'ОТ 1 082 000 руб.'}
    ],
    accordeon: function (fullData) {
        return fullData.accordeon.catalog;
    }
}};
